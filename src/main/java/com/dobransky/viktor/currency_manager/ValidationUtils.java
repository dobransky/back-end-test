package com.dobransky.viktor.currency_manager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils
{
    private static final Pattern allowedInputRegex = Pattern.compile("[A-Z]{3} -?[0-9]{1,19}$");

    public static boolean validateInput(String inputLine)
    {
        Matcher matcher = allowedInputRegex.matcher(inputLine);
        if (matcher.matches())
        {
            return true;
        }

        return false;
    }
}

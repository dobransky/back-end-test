package com.dobransky.viktor.currency_manager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class CurrencyHandler implements Runnable
{
    private Map<String, Long> currenciesMap;
    private Thread thread;
    private boolean run;
    private static final long REPETITION_TIME = 60000;

    private static final Logger logger = Logger.getLogger(CurrencyHandler.class);

    public CurrencyHandler()
    {
        this.currenciesMap = Collections.synchronizedMap(new HashMap<String, Long>());

        run = true;

        thread = new Thread(this, "Currency handler");
        thread.start();
    }

    public void addCurrency(String currency, long amount)
    {
        synchronized (currenciesMap)
        {
            Long value = 0L;
            if (currenciesMap.containsKey(currency))
            {
                value = currenciesMap.get(currency);
            }

            value += amount;

            currenciesMap.put(currency, value);

            synchronized (System.out)
            {
                logger.info(String.format("Payment '%s %s' was succesfully added to list", currency, amount));
            }
        }
    }

    @Override
    public void run()
    {
        while (run)
        {
            synchronized (currenciesMap)
            {
                if (!currenciesMap.isEmpty())
                {
                    synchronized (System.out)
                    {
                        System.out.println("All payments\n----------");

                        for (String currencyName : currenciesMap.keySet())
                        {
                            Long value = currenciesMap.get(currencyName);
                            if (value != 0L)
                            {
                                System.out.println(String.format("%s %s", currencyName, value));
                            }
                        }

                        System.out.println("----------");
                    }
                }
            }

            try
            {
                Thread.sleep(REPETITION_TIME);
            }
            catch (InterruptedException e)
            {
            }
        }
    }

    public void stop()
    {
        run = false;
        thread.interrupt();
    }

    public Thread getThread()
    {
        return thread;
    }
}

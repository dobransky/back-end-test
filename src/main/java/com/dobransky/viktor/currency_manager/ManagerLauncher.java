package com.dobransky.viktor.currency_manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * @author Dobransky Viktor
 * 
 */
public class ManagerLauncher
{

    private static final Logger logger = Logger.getLogger(ManagerLauncher.class);

    private static final String PAYMENT_SEPARATOR = " ";

    public static void main(String[] args)
    {
        logger.info("Start parsing arguments");

        CommandLineOptions options = new CommandLineOptions();
        CmdLineParser cmdParser = new CmdLineParser(options);

        try
        {
            cmdParser.parseArgument(args);
        }
        catch (CmdLineException e)
        {
            logger.error("Error while parsing arguments.");
            System.exit(-1);
        }

        CurrencyHandler handler = new CurrencyHandler();
        if (options.inputFile != null)
        {
            try
            {
                parseFile(options.inputFile, handler);
                logger.info("Input file readed succesfully.");
            }
            catch (Exception e1)
            {
                logger.error("Error while reading input file.");
            }
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        System.out.println("Ready to read your payments. For quit the application write 'quit' and press ENTER.");
        while (true)
        {
            try
            {
                input = br.readLine();

                if ("quit".equalsIgnoreCase(input))
                {
                    break;
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            if (input != null && !input.isEmpty())
            {
                boolean validate = ValidationUtils.validateInput(input);

                if (validate)
                {
                    String[] currency = input.trim().split(PAYMENT_SEPARATOR);
                    handler.addCurrency(currency[0], Long.parseLong(currency[1]));
                }
                else
                {
                    System.out.println("Wrong format of currency. It should be like this: USA 100");
                }
            }
        }

        handler.stop();

        try
        {
            handler.getThread().join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        logger.info("Application has been shutdown succesfully.");
    }

    public static void parseFile(File file, CurrencyHandler handler) throws IOException
    {
        BufferedReader br = null;
        String line = "";

        try
        {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    continue;
                }

                boolean validate = ValidationUtils.validateInput(line);
                if (validate)
                {
                    String[] currencyAmount = line.trim().split(PAYMENT_SEPARATOR);
                    handler.addCurrency(currencyAmount[0], Long.parseLong(currencyAmount[1]));
                }
                else
                {
                    System.out.println(String.format("Wrong format of payment: ", line));
                }
            }
        }
        catch (FileNotFoundException e)
        {
            throw e;
        }
        catch (IOException e)
        {
            throw e;
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class CommandLineOptions
    {
        @Argument(index = 0, usage = "path to the input file", metaVar = "Path to input file", required = false)
        private File inputFile;
    }
}

package utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.dobransky.viktor.currency_manager.ValidationUtils;

public class ValidationUtilsTest
{

    @Test
    public void wrongInputForCurrencyNameTest()
    {
        boolean validate = ValidationUtils.validateInput("GB1 200");
        assertTrue(validate == false);
    }

    @Test
    public void wrongInputForCurrencyValueTest()
    {
        boolean validate = ValidationUtils.validateInput("GBR e22");
        assertTrue(validate == false);
    }

    @Test
    public void wrongSmallCountOfCurrencyCharactersTest()
    {
        boolean validate = ValidationUtils.validateInput("GB 100");
        assertTrue(validate == false);
    }

    @Test
    public void wrongLargerCountOfCurrencyCharactersTest()
    {
        boolean validate = ValidationUtils.validateInput("GBRN 100");
        assertTrue(validate == false);
    }

    @Test
    public void onlyOneWordTest()
    {
        boolean validate = ValidationUtils.validateInput("GBRN");
        assertTrue(validate == false);
    }

    @Test
    public void corectInputTest()
    {
        boolean validate = ValidationUtils.validateInput("GBR 100");
        assertTrue(validate);
    }

    @Test
    public void wrongInputSmallNameTest()
    {
        boolean validate = ValidationUtils.validateInput("gbr 100");
        assertTrue(validate == false);
    }

    @Test
    public void corectInputLongValueTest()
    {
        boolean validate = ValidationUtils.validateInput("GBR 1000000000000000");
        assertTrue(validate);
    }

    @Test
    public void wrongInputLongValueTest()
    {
        boolean validate = ValidationUtils.validateInput("GBR 10000000000000000000000000");
        assertTrue(validate == false);
    }

    @Test
    public void corectNegativeValueInputTest()
    {
        boolean validate = ValidationUtils.validateInput("GBR -100");
        assertTrue(validate);
    }
}

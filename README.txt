Hello .
This is application to manage your payments.
You will need Java 6 and to build source codes use Maven.
To run program write this to your command line when you are set on release directory:
java -jar currency-manager.jar
or you can use your input file with all your payments as well:
java -jar currency-manager.jar input.txt
You can add your payments by taping currency (3 alphabets) and the price (positive or negative).
To quit the application just write quit and press ENTER.

Regards
Viktor Dobransky